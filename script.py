#!/usr/bin/env python3

# Affiche un message à l'écran
print("Hello 3SRC !")

# On demande un nombre
nbr = int(input('Entrez un nombre : '))
#Initialise la variable
fact = 1
# On vérifit que la réponse est supérieur à 0
if nbr < 0 :
    print('Entrez un chiffre supérieur à 0')
# Si la réponse est égal à 0
elif nbr == 0 :
    print('Le factoriel de 0 est 1 par convention')
else :
# Range() est une fonction native pour généré des suites arithmétiques sur le nombre on commence par 1 avec le premier argument.
    for i in range(1, nbr+1):
        # On multiplie i (la récurrence/boucle) au premier tour par 1 puis le résultat précéndant contenu dans la variable fact.
        fact = fact * i
    print ('Le factoriel de',nbr,'est',fact,'!')