#!/usr/bin/env python3

import pandas

df = pandas.read_csv('data.csv')

print("Tableau des prénoms de ",df["Annee"].min(),"à",df["Annee"].max())
print(df,"\n")

# Les deux prénomes les plus utilisés depuis 
df2=df.nlargest(2,['Nombre'])
print("Prenom le plus utilisé")
print(df2,"\n")

# Using DataFrame.nsmallest() function.
df3=df.nsmallest(2,['Nombre'])
print("Prenom le moins utilisé")
print(df3,"\n")
